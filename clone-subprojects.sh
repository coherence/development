#!/bin/sh

SUBPROJECTS="Cadre Coherence Compere Mirabeau Puncher UPnP-Inspector"

origin=$(git remote -v | egrep 'coherence-project/Development.* \(fetch\)')
origin=$(echo $origin | cut -d ' ' -f 2)

for sp in $SUBPROJECTS ; do
    spurl=${origin/\/Development./\/$sp.}
    if [ ! -d "$sp" ] ; then
	git clone "$spurl"
    else
	echo "Updating $sp:"
	( cd "$sp" ; git pull )
    fi
done
