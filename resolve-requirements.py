#!/usr/bin/env python

import pkg_resources
import sys
import os
import glob
import imp

glob_patterns = ('*.egg*', '*.pth')

def process_pth_file(pth):
    base = os.path.dirname(pth)
    with open(pth) as fh:
        for line in fh:
            if line.startswith("#"):
                continue
            if line.startswith("import"):
                continue
            line = line.rstrip()
            dir = os.path.join(base, line)
            if os.path.exists(dir):
                yield dir


def search_on_pythonpath(name):
    #print 'search on py-path:', name
    mod_name = name.split('.', 1)[0]
    try:
        mod = imp.find_module(mod_name)[1]
    except ImportError, e:
        raise SystemExit(str(e))
    yield mod
    pth = os.path.splitext(mod)[0] + os.extsep + 'pth'
    if os.path.exists(pth):
        yield pth
        for p in process_pth_file(pth):
            yield p


def get_distribution(requirement):
    # search without defaults already on sys.path
    # modelled after main-part of pkg_resources
    working_set = pkg_resources.WorkingSet([])
    dist = working_set.resolve(pkg_resources.parse_requirements(requirement),
                               pkg_resources.Environment())
    if dist:
        return dist[0]
    return None


def resolve(name):
    try:
        dist = get_distribution(name)
    except pkg_resources.DistributionNotFound:
        for n in search_on_pythonpath(name):
            yield n
        return
    #print 'found', dist
    if dist.location.endswith('egg'):
        # this is an .egg-file or an .egg-dir, everything is contained here
        yield dist.location
        return

    # collect all *.egg an *.pth files for this egg
    #print dist.location
    globbase = os.path.join(dist.location, dist.egg_name())
    for pat in glob_patterns:
        for f in glob.iglob(globbase + pat):
            yield f
    # look for top-level files/dists
    egg_info = os.path.join(dist.location, dist.egg_name()+ '.egg-info')
    try:
        with open(os.path.join(egg_info, 'top_level.txt')) as infh:
            for top_level in infh:
                top_level = top_level.strip()
                if not top_level:
                    continue
                yield os.path.join(dist.location, top_level)
    except IOError:
        search_on_pythonpath(name)
        pass

for name in sys.argv[1:]:
    for l in resolve(name):
        print l
