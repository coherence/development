#!/bin/bash

VENV_DIR=./_venv
CACHE=$(dirname "$0")/.pip-install-cache

SUBPROJECTS=(Coherence Cadre Compere Mirabeau Puncher UPnP-Inspector)
SUBPROJECTS=(Coherence UPnP-Inspector)
#SUBPROJECTS=(Coherence)

REQS="Twisted-Core twisted zope.interface pygtk gobject glib cairo numpy"

# read the paths required to be linked into an array
# (For some obscure reason, `read` does not read from a pipe. So we
# need a tmp-file here)
tmp=$(mktemp)
python ./resolve-requirements.py $REQS > $tmp
IFS=$'\n' read -d '' -r -a requirements < $tmp
rm $tmp

export PYTHONPATH=
virtualenv "$VENV_DIR"
source "$VENV_DIR"/bin/activate

platlib=$(python -c 'import sysconfig ; print sysconfig.get_paths()["platlib"]')

for req in "${requirements[@]}" ; do
    bname=$(basename "$req")
    if [ -L "$platlib/$bname" ] ; then
	echo >&2 "link exists: $req"
    else
	echo 'linking' $req
	ln -s "$req" $platlib
    fi
done

# The rpm-package installs `Twisted_Core`, but PyPi only knows
# `Twisted`. So we fake the package as being installed by setting
# another link.
twisted_core=$(ls $platlib/Twisted_Core-*.egg-info)
twisted=${twisted_core/_Core}
if [ -L "$twisted" ] ; then
    echo >&2 "link exists: $twisted"
else
    echo 'linking' $twisted
    ln -s "$twisted_core" "$twisted"
fi


fake_egg_info () {
    local name=$1 ; shift
    local ver=$1 ; shift
    local dir=$platlib/$name-$ver.egg-info
    mkdir $dir
    echo > $dir/PKG-INFO <<"EOF"
Metadata-Version: 1.0
Name: $name
Version: $ver
EOF
}

# Create a fake egg-info entry for pygtk to avoid pip trying to
# install it
fake_egg_info pygtk 2.24


# need to call pip separatly for each requirement to ensure
# installation order
for req in "${SUBPROJECTS[@]}" ; do
    pip install --download-cache $CACHE -e ${req/#/./}
done

# Local Variables:
# ispell-local-dictionary: "american"
# End:
