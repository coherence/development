================================
Tools for Developing Coherence
================================

**This tools help you developing Coherence.**

Since Coherence requires some extensions which are not pure-python,
setting up a development environment can be cumbersome. This tools
help you setting up and maintaining the development environment.

:clone-subprojects.sh: Clones the sub-projects

   - Coherence
   - Cadre
   - Compere
   - Mirabeau
   - Puncher
   - UPnP-Inspector

   Please note: The following projects will *not* be cloned, as they
   do not need the Coherence  Framework.

   - Coherence-Config

:setup-virtualenv.sh: Sets up a Python virtualenv containing all the
   sub-projects.


*More to come ...*

..
  Local Variables:
  mode: rst
  ispell-local-dictionary: "american"
  End:
